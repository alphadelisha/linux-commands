#! /bin/bash

echo "INSTALL KUBECTL.." 

echo "1.Download the latest release"
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
echo "Done.."

echo "2.Make the kubectl binary executable"
chmod +x ./kubectl
echo "Done.."

echo "3.Move the binary in to your PATH"
sudo mv ./kubectl /usr/local/bin/kubectl
echo "Done.."

echo "4.Test to ensure the version you installed is up-to-date"
kubectl version --client

echo "INSTALL VIRTUALBOX"

echo "1.Install required linux headers"
sudo apt-get -y install gcc make linux-headers-$(uname -r) dkms
echo "Done.."

echo "2.Add VirtualBox Repository and key"
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
echo "Done.."

echo "3.Add Virtual repository to your system"
sudo sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" >> /etc/apt/sources.list'
echo "Done.."

echo "4.Install VirtualBox"
sudo apt-get update && sudo apt-get install virtualbox
echo "Done.."

echo "5.Verify if VirtualBox is installed"
VBoxManage -v

echo "INSTALL MINIKUBE"
echo ""
echo "1.Install Minikube via direct download"
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube
echo "Done.."
echo ""
echo "2.Add Minikube to your path"
sudo mkdir -p /usr/local/bin/ && sudo install minikube /usr/local/bin/
echo "Done.."
