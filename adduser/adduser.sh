#! /usr/bin/env sh

# add user with name alpha, change the name with user name what you want
adduser alpha

# add sudo previllage to new user, change the name with user name what you want.
usermod -aG sudo alpha

# remove default authorized_keys for user root
rm /root/.ssh/authorized_keys
